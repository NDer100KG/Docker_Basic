# check tensorflow cuda
import tensorflow as tf

print('==========================================')
good = tf.test.is_gpu_available()
print('TF test result: ', good)
print('==========================================')

# check torch cuda
import torch 

print('==========================================')
good = torch.cuda.is_available()
print('torch test result: ', good)
print('==========================================')