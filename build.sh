repo_name="nder100kg"
docker_name="tf_torch_cuda10.1"

## build image from Dockerfile
docker build -t=${repo_name}/${docker_name} .

## Start a container from image with name
# docker run --runtime=nvidia -it --name=nder ${repo_name}/${docker_name} 

docker run  --runtime=nvidia -it \
            --name=XBM_con_1 \
            -v /home/nder/Documents/tf_XBM/:/tf_XBM \
            -v /media/nder/KingstonSSD/DML_dataset/CUB/:/data/CUB \
            --shm-size 24G \
            nder100kg/tf_torch_cuda10.1:setup