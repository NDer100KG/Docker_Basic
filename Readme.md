# Nvidia Docker with basic deep learning tools
build nvidia-docker with basic deep learning tools installed with miniconda

* Basic installation
    see [link](https://medium.com/@william._./nvidia-docker2-%E5%AE%89%E8%A3%9D%E5%8F%8A%E4%BD%BF%E7%94%A8-50f564b1f5af) [ubuntu18.04](https://gist.github.com/lamhoangtung/d19bb72a99639a762b6d935fbd080c7c)[ubuntu16.04](https://blog.csdn.net/chenmaolin88/article/details/86242997)

* install nvidia-docker2
    ```
    curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
    distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
    curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
    sudo apt-get update

    sudo apt-get install -y nvidia-docker2
    sudo pkill -SIGHUP dockerd

    docker run --runtime=nvidia --rm nvidia/cuda:10.2-base-ubuntu18.04 nvidia-smi
    ```

* docker flow:

    <img src=pic/docker-flow_0.png width=80%>

    <img src=pic/docker-flow_1.jpg width=80%>

* notes:
    * Show all containers
        ```
        echo $(docker ps -a -q)
        ```
    * Stop all containers
        ```
        docker stop $(docker ps -a -q)
        ```
    * Remove all containers
        ```
        docker rm $(docker ps -a -q) 
        ```
    * export container => exporting "CONTAINER"
        ```
        docker export {CONTAINER_NAME} > {EXPORT_FILENAME}.tar
        ```
    * Start a container from image with name
        ```
        docker run --runtime=nvidia -it -v ${LOCAL_PATH}:${REMOTE_PATH} --name=nder ${repo_name}/${docker_name} 
        ```
    * build image from Dockerfile
        ```
        docker build -t=${repo_name}/${docker_name} .
        ```
    * commit container to image
        ```
        docker commit nder {IMAGE_REPO}:{TAG}
        ```
    * save image => save "IMAGE"
        ```
        docker save -o {SAVE_FILENAME}.tar {IMAGEID}
        ```
    * load image
        ```
        docker load -i {SAVE_FILENAME}.tar
        ```
    * copy file into container:
        ```
        docker cp /path/to/file1 DOCKER_ID:/path/to/file2
        ```
    * clean docker build cache
        ```
        docker builder prune -af
        ```
    * see docker disk usage
        ```
        docker system df
        ```

* Docker Registry
    * WebUI: [mkuchin/docker-registry-web](https://github.com/mkuchin/docker-registry-web)
        * [Install Docker-compose](https://docs.docker.com/compose/install/)
        * see [docker-registry-web/examples/auth-enabled/README.md](https://github.com/mkuchin/docker-registry-web/blob/master/examples/auth-enabled/README.md)
    * Docker push to registry and pull, Example: 
        ```
        docker tag ubuntu:16.04 <IP>:5000/ubuntu:16.04

        docker login <IP>:5000 // should see success
        docker push <IP>:5000/ubuntu:16.04
        docker pull <IP>:5000/ubuntu:16.04
        ```
    *   If encounter `Get https://0.0.0.0:5000/v2/: http: server gave HTTP response to HTTPS client`:
        ```
        sudo gedit /etc/docker/daemon.json
        // add follwing lines
        "live-restore": true,
        "group": "dockerroot",
        "insecure-registries": ["0.0.0.0:5000"]

        // restart docker service
        systemctl restart docker
        ```

* Error notes:

    * shared memory: [torch dataloader error](https://zhuanlan.zhihu.com/p/143914966) 
        ```
        Add --shm-size within docker run 
        ```
    * if encountered `docker: Error response from daemon: Unknown runtime specified nvidia`:
        ```
        sudo systemctl daemon-reload
        sudo systemctl restart docker
        ```
    * docker running without sudo:
        ```
        sudo groupadd docker
        sudo gpasswd -a $USER docker
        log out and re-login
        docker run hello-world
        ```
    * To use display, add following lines when docker run:
        ```
        -e DISPLAY=${DISPLAY} 
        -v /tmp/.X11-unix/:/tmp/.X11-unix/
        ```

        and also give access to docker by:
        ```
        xhost +local:docker
        ```
    * offline install vscode-server([link](https://zhuanlan.zhihu.com/p/294933020)):
        ```
        commit_id = 379476f0e13988d90fab105c5c19e7abc8b1dea8
        1. https://http://update.code.visualstudio.com/commit:${commit_id}/server-linux-x64/stable（注意把:${commit_id}替换成对应的Commit ID）下载vscode-server-linux-x64.tar.gz。
        2. mkdir -p ~/.vscode-server/bin
        3. rm ~/.vscode-server/bin/* -rf
        4. cd ~/.vscode-server/bin
        5. tar -zxf vscode-server-linux-x64.tar.gz
        6. mv vscode-server-linux-x64 ${commit_id}
        ```

    
