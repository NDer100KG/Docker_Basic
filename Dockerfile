FROM nvidia/cuda:10.1-base

ARG miniconda_file=Miniconda3-latest-Linux-x86_64.sh

### Make sure apt is in correct status and install gcc
RUN apt update && \
    apt install -y gcc

ADD environment.yml /tmp/environment.yml
ADD $miniconda_file /tmp/$miniconda_file
ADD check_cuda.py /tmp/check_cuda.py

### Install miniconda
RUN bash /tmp/$miniconda_file -b && \
    echo "export PATH="/root/miniconda3/bin:$PATH"" >> ~/.bashrc && \
    /bin/bash -c "source ~/.bashrc"
ENV PATH /root/miniconda3/bin:$PATH

### create conda env using environment.yml
RUN conda env create -f /tmp/environment.yml

RUN echo "source activate $(head -1 /tmp/environment.yml | cut -d' ' -f2)" > ~/.bashrc && \
    /bin/bash -c "source ~/.bashrc"

# ENTRYPOINT nvidia-smi